package testing.tdd.practico.ejercicio5;

import testing.tdd.practico.ejercicio3.Primo;
import testing.tdd.practico.ejercicio4.Criba;


import java.util.ArrayList;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;



import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class PrimoDiferencialTest {

	private static final int CANTINT = 25;

	@ParameterizedTest(name = "{index}: Generar {0} primos")
	@MethodSource("intProvider")
	void findLastParamTest(int n) {
		assertArrayEquals(Primo.primos(n), Criba.primos(n));
	}
	
	static Stream<Arguments> intProvider() {
		ArrayList<Arguments> res = new ArrayList<Arguments>();
		for (int i = -2;i < CANTINT; i++) {
			res.add(Arguments.of(i));
		}
	    return res.stream();
	}

	@Test
	public void sietePrimosTest(){
		int[] res = {1,2,3,5,7,11,13};
		assertArrayEquals(res, Primo.primos(7));
	}
}
