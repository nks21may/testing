package testing.tdd.practico.ejercicio3;


import static org.junit.jupiter.api.Assertions.assertArrayEquals;


import org.junit.jupiter.api.Test;

/*
 * Explicacion de la notacion [I.K]
 * La I indica que vuelta es de la metodologia TDD
 * La K indica el paso del TDD..
 * 		1. Escribiendo test que falle
 *		2. Escribiendo codigo haga pasar el test
 *		3. Refactorizar
*/

public class PrimoTest {

	//[1.1]
	@Test
	public void zeroPrimosTest(){
		int[] res = {};
		assertArrayEquals(res, Primo.primos(0));

	}

	//[2.1]
	@Test
	public void unoPrimosTest(){
		int[] res = {1};
		assertArrayEquals(res, Primo.primos(1));

	}
	@Test
	public void dosPrimosTest(){
		int[] res = {1,2};
		assertArrayEquals(res, Primo.primos(2));

	}
	@Test
	public void tresPrimosTest(){
		int[] res = {1,2,3};
		assertArrayEquals(res, Primo.primos(3));
	}
	@Test
	public void sietePrimosTest(){
		int[] res = {1,2,3,5,7,11,13};
		assertArrayEquals(res, Primo.primos(7));
	}
}
