package testing.tdd.practico.ejercicio2;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;


public class TriangleTest {

	@Test
	public void testBadArguments1() {
		assertEquals(Triangle.Invalid, Triangle.clasific(-1,-1,-1));
	}

	@Test
	public void testBadArguments2() {
		assertEquals(Triangle.Invalid, Triangle.clasific(-1, 1, 1));
	}
	
	@Test
	public void testBadArguments3() {
		assertEquals(Triangle.Invalid, Triangle.clasific(1, 1,-1));
	}

	@Test
	public void testBadArguments4() {
		assertEquals(Triangle.Invalid, Triangle.clasific(1, -1, 1));
	}
	
	//2do codigo	
	@Test
	public void testEscalen() {
		assertEquals(Triangle.Escalen, Triangle.clasific(1, 2, 3));
	}

	@Test
	public void testIsoscel() {
		assertEquals(Triangle.Isoscel, Triangle.clasific(2, 2, 3));
	}

	@Test
	public void testEquilat() {
		assertEquals(Triangle.Equilat, Triangle.clasific(2, 2, 2));
	}

}
