package testing.tdd.practico.ejercicio2;

import java.util.Arrays;
import java.util.List;

public class Triangle {

	public static final String Equilat = "Equilat";
	public static final String Isoscel = "Isoscel";
	public static final String Escalen = "Escalen";
	public static final String Invalid = "Invalid";

	public static String clasific(int a, int b, int c){
		if (a <= 0 || b <= 0 || c <= 0) return Invalid;


		//2do codigo
		int r = 0;
		String res = "";
		
		if (a == b) r++;
		if (a == c) r++;
		if (b == c) r++;

		switch (r) {
			case 0 : res = Triangle.Escalen;
				break;
			case 1: res = Triangle.Isoscel;
				break;
			default: res = Triangle.Equilat;
				break;
		}

		return res;
	}	

}