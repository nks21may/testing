package testing.tdd.practico.ejercicio3;

public class Primo {

	private static boolean esPrimo(int n){
		//[2.2]
		if(n == 1) return true;
		//[2.2]
		int flag = 0;

		for (int i = 1;i < n; i++)
			if(n % i == 0) 
				flag++;
		
		return flag == 1;
	}

	public static int[] primos(int n){
		if(n < 0) return new int[0]; //Parche con lo que genero el ejercicio 5		

		//[1.2]
		int[] res = new int[n];
		int i = 1;
		int count = 0;
		while(count < n) {
			if(esPrimo(i)){
				res[count] = i;
				count++;
			}
			i++;
		}

		return res;
		//[1.2]
	}
}