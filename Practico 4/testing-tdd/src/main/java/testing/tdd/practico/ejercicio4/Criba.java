package testing.tdd.practico.ejercicio4;

import java.util.ArrayList;
import java.util.List;

public class Criba {

	private static boolean continua(int n, int p){
		return Math.pow(p,2) < n;
	}

	private static ArrayList<Integer> loadInt(ArrayList<Integer> primos, int min, int max){
		ArrayList<Integer> l = new ArrayList<Integer>();
		for (int i = min;i < max; i++) {
			final int k = i;
			if(!primos.stream().anyMatch((x) -> k % x == 0)){
				l.add(i);
			}
		}
		return l;
	}

	static int max = 100;

	//return nprimos
	public static int[] primos(int n){
		//[1.1]
		if(n <= 0){
			int[] res = {};
			return res;	
		}
		if (n == 1){
			int[] res = {1};
			return res;
		}
		ArrayList<Integer> primos = new ArrayList<Integer>();
		ArrayList<Integer> l = loadInt(primos, 2, max);
		primos.add(2);
		int lap = 2;
		//aclaracion: por querer usar la expresion lamda en el remove if tuve que poner ese final
		while(n-1 > primos.size()){
			final int lastPrimo = (l.isEmpty())? lap : l.remove(0);
			lap = lastPrimo;
			if(!primos.contains(lastPrimo)){
				primos.add(lastPrimo);
			}
			
			if(l.size() < 5){
				int aux = l.get(l.size()-1);
				l.addAll(loadInt(primos, max, max+100));
				max += 100;
			}
			l.removeIf( x -> x % lastPrimo == 0);
		}
		primos.add(0, 1);
		return converter(primos);
	}

	public static int[] converter(List<Integer> l){
		int[] res = new int[l.size()];
		for (int i = 0;i < l.size(); i++) {
			res[i] = l.get(i);
		}
		return res;
	}
}