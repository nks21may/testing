package testing.tdd.practico.ejercicio1;

import java.util.Arrays;
import java.util.List;

public class SimpleCalc {

static String delimiter = "";

public static String findDelimiter(String numbers){
	delimiter = ",";
	if(numbers.length() < 3) return numbers;

	if(numbers.startsWith("//")){
		delimiter = numbers.substring(2, numbers.indexOf​("\n"));
		numbers = numbers.substring(3,numbers.length());
	}
	return numbers;
}

	
public static int add(String numbers) {
		if(numbers == null || numbers == "") return 0;
		numbers = findDelimiter(numbers);
		int res = 0;
		System.out.println(delimiter);
		//for (String s: numbers.split(",")) { 1er codigo
		//for (String s: numbers.split(",|\\n")) { 2do codigo
		for (String s: numbers.split(delimiter+"|\\n")) { 
			if(!"".equals(s)){
				res += Integer.parseInt(s);
			}
		}

		return res;
	}
}
