//Introduction to Software Testing
//Authors: Paul Ammann & Jeff Offutt

package test.testing.junit.ejercicio1;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.*;

import testing.junit.practico.ejercicio1.Ejercicio1;

import java.util.*;

public class Ejercicio1Test {

	/*
	 * Si es posible, provea un test JUnit que no ejecute el defecto 2. Si es
	 * posible, provea un test JUnit que ejecute el defecto y no resulte en una
	 * falla 3. Si es posible, provea un test JUnit que ejecute el defecto y
	 * produzca una falla. 4. Explique detalladamente que sucedió para que la
	 * falla pueda detectarse en términos del modelo RIPR visto en la teoría. 5.
	 * Corrija el defecto y vuelva a ejecutar los tests anteriores hasta que
	 * todos sean exitosos Además, escriba como mínimo dos tests negativos (ej.
	 * que devuelvan excepciones).
	 */


	@Test
	public void testFindLastNormal() {
		int[] x = {1,2,3};
		
		assertTrue(Ejercicio1.findLast(x, 2) == 1, "Normal test");
//		fail("NullPointerException expected");*/
	}
	
	@Test
	public void testFindLastNull() {
		assertThrows(NullPointerException.class, () -> Ejercicio1.findLast(null, 2) );
	}

	@Test
	public void testFindLastZero() {
		int[] x = {1,2,3};
		
		assertTrue(Ejercicio1.findLast(x, 1) == 0, "Zero test");
	}
	//========================

	@Test
	public void testlastZeroNormal() {
		int[] x = {0,2,0};
		
		assertTrue(Ejercicio1.lastZero(x) == 2, "Normal test");
//		fail("NullPointerException expected");*/
	}

	@Test
	public void testlastZeroNull() {
		assertThrows(NullPointerException.class, () -> Ejercicio1.lastZero(null) );
	}

	@Test
	public void testlastZeroZero() {
		int[] x = {1,2,3};
		
		assertTrue(Ejercicio1.lastZero(x) == -1, "Zero test");
	}

	//==========================

	@Test
	public void testnumZeroNormal() {
		int[] x = {0,2,0,3,0};
		
		assertTrue(Ejercicio1.numZero(x) == 3, "Normal numZero test");
		//fail("NullPointerException expected");*/
	}

	@Test
	public void testnumZeroNull() {
		assertThrows(NullPointerException.class, () -> Ejercicio1.numZero(null));
	}

	@Test
	public void testnumZeroZero(){
		int[] x = {1,2,3};
		
		assertTrue(Ejercicio1.numZero(x) == 0, "Zero numZero test");
	}

	//==========================

	@Test
	public void testoddOrPosNormal() {
		int[] x = {0,1,2,3,4,-1,-2,-3,-4};
		
		assertTrue(Ejercicio1.oddOrPos(x) == 6, "Normal oddOrPos test");
		//fail("NullPointerException expected");*/
	}

	@Test
	public void testoddOrPosNull() {
		assertThrows(NullPointerException.class, () -> Ejercicio1.oddOrPos(null));
	}

	@Test
	public void testoddOrPosZero(){
		int[] x = {-1,-2,-3};
		
		assertTrue(Ejercicio1.oddOrPos(x) == 2, "Zero oddOrPos test");
	}

	@Test
	public void testoddOrPosZero2(){
		int[] x = {1,2,3};
		
		assertTrue(Ejercicio1.oddOrPos(x) == 3, "Zero oddOrPos test");
	}


	//==========================

	@Test
	public void testcountPositiveNormal() {
		int[] x = {0,1,2,3,4,-1,-2,-3,-4,2};
		
		assertTrue(Ejercicio1.countPositive(x) == 5, "Normal countPositive test");
		//fail("NullPointerException expected");*/
	}

	@Test
	public void testcountPositiveNull() {
		assertThrows(NullPointerException.class, () -> Ejercicio1.countPositive(null));
	}

	@Test
	public void testcountPositiveZero(){
		int[] x = {0,-1,-2,-3};
		
		assertTrue(Ejercicio1.countPositive(x) == 0, "Zero countPositive test");
	}
}