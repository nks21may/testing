//Introduction to Software Testing
//Authors: Paul Ammann & Jeff Offutt

package test.testing.junit.ejercicio1;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.*;

import testing.junit.practico.ejercicio2.BoundedQueue;

import java.util.*;

public class BoundedQueueTest {

  private BoundedQueue queue;   // Test fixture

   @BeforeEach      // Set up - Called before every test method.
   public void setUp() {
      queue = new BoundedQueue(5);
   }

   @AfterEach      // Tear down - Called after every test method.
   public void tearDown() {
      queue = null;  // redundant in this example!
   }

    @Test
    public void testForNullList() {
		queue = new BoundedQueue(0);
    	assertThrows(IllegalStateException.class, () -> queue.enQueue(5));
	}

   // assertThrows is now the preferred mechanism for testing exceptions
   // note the lambda expression in the assertion
   @Test 
   public void testForNullElement() {
   	  queue.enQueue("cat");
      assertThrows(NullPointerException.class, () -> queue.enQueue(null));
   }

   @Test
   public void testForSoloNullElement() {
      assertThrows(NullPointerException.class, () -> queue.enQueue(null));
   }

   @Test
   public void testDeQueue() {
      queue.enQueue((Object) "cat");
      queue.enQueue((Object) "dog");
      assertTrue("cat".equals(queue.deQueue()), "Single element dequeue");
   }

   @Test
   public void testEmptyList() {
      assertThrows(IllegalStateException.class, () -> queue.deQueue());
   }
   
   @Test
   public void testisEmpty() {
     assertTrue (queue.isEmpty(), "Single empty queue");
   }

   @Test
   public void testisFull() {
     assertTrue (!queue.isFull(), "Single empty queue");
   }
}