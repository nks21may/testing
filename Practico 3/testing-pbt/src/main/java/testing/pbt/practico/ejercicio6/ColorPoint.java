package testing.pbt.practico.ejercicio6;

//Introduction to Software Testing
//Authors: Paul Ammann & Jeff Offutt

public class ColorPoint extends Point 
{
   private Color color;
   // Fault: Superclass instantiable; subclass state extended
 
   public ColorPoint(int x, int y, Color color) { 
      super (x,y);
      this.color=color; 
   } 

   @Override public boolean equals(Object o) { 
      // Location B
      if (o == null) return false;
      if (!(o.getClass() == getClass())) return false;

      ColorPoint cp = (ColorPoint) o;
      return super.equals(o) && color.equals(cp.color);
   }
}

// https://agiledeveloper.com/articles/equals062002.htm
enum Color {RED, WHITE, BLUE}