package testing.pbt.practico.ejercicio4;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assumptions.assumeThat;

import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class Ejercicio4Test{

	static Stream<Arguments> simpleProvider() {
	    return Stream.of(
	    		Arguments.of(new int[] {0,2,3}),
	    		Arguments.of(new int[] {1,2,3}),
	    		Arguments.of(new int[] {0}),
	    		Arguments.of(new int[] {1,0,3}),
	    		Arguments.of(new int[] {0,0,0}),
	    		Arguments.of(new int[] {-1,-2,-3}),
	    		Arguments.of(new int[] {-2,0,2}),
	    		Arguments.of(new int[] {2,0,-2}),
	    		Arguments.of(new int[] {2,-2,-2}),
	    		Arguments.of(new int[] {2,-3,-3}),
	    		Arguments.of(new int[] {1,2,0})
	    );
	}

	@ParameterizedTest()
	@MethodSource("simpleProvider")
	void findLastEQlastZeroProperty(int [] arr) {
		int resultFindLast = Ejercicio1.findLast(arr, 0);
		int resultLastZero = Ejercicio1.lastZero(arr);
		assertThat(resultFindLast).isEqualTo(resultLastZero);
	}

	//la cantidad de countpositive es menor o igual a oddorPos
	@ParameterizedTest()
	@MethodSource("simpleProvider")
	void oddorPosAndlastZeroProperty(int [] arr) {
		int resultOddOrPos = Ejercicio1.oddOrPos(arr);
		int resultCountPositive = Ejercicio1.countPositive(arr);
		assertThat(resultOddOrPos).isGreaterThanOrEqualTo(resultCountPositive);
	}

	@ParameterizedTest()
	@MethodSource("simpleProvider")
	void zeroesNotPositive(int [] arr) {
		int resultnumZero = Ejercicio1.numZero(arr);
		int resultCountPositive = Ejercicio1.countPositive(arr);
		assertThat(arr.length).isGreaterThanOrEqualTo(resultnumZero + resultCountPositive);
	}

}
