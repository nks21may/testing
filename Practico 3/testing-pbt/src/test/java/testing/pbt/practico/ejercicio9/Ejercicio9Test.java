package testing.pbt.practico.ejercicio9;

import testing.pbt.practico.ejercicio9.Ejercicio9;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assumptions.assumeThat;

import net.jqwik.api.ForAll;
import net.jqwik.api.Property;
import net.jqwik.api.constraints.IntRange;
import net.jqwik.api.constraints.Size;

public class Ejercicio9Test{

	@Property
	void findLastEQlastZeroProperty(@ForAll @Size(max=20) @IntRange(min=-5, max=5) int [] arr) {
		int resultFindLast = Ejercicio9.findLast(arr, 0);
		int resultLastZero = Ejercicio9.lastZero(arr);
		assertThat(resultFindLast).isEqualTo(resultLastZero);
	}

	@Property
	void oddorPosAndCountPositiveProperty(@ForAll @Size(max=100) @IntRange(min=-100, max=100) int [] arr) {
		int resultOddOrPos = Ejercicio9.oddOrPos(arr);
		int resultCountPositive = Ejercicio9.countPositive(arr);
		assertThat(resultOddOrPos).isGreaterThanOrEqualTo(resultCountPositive);
	}

	@Property
	void zeroesNotPositive(@ForAll @Size(max=100) @IntRange(min=-100, max=100) int [] arr) {
		int resultnumZero = Ejercicio9.numZero(arr);
		int resultCountPositive = Ejercicio9.countPositive(arr);
		assertThat(arr.length).isGreaterThanOrEqualTo(resultnumZero + resultCountPositive);
	}

}
