package testing.pbt.practico.ejercicio6;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;


import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

//Introduction to Software Testing
//Authors: Paul Ammann & Jeff Offutt

public class ColorPointTest {

    @ParameterizedTest(name = "{index}: Reflexive ColorPoint {0}")
    @MethodSource("reflexiveProvider")
    public void reflexive(ColorPoint p){
        assertTrue(p.equals(p));
    }

    @ParameterizedTest(name = "{index}: Symmetry ColorPoint {0} {1}")
    @MethodSource("symmetryProvider")
    public void symmetry(ColorPoint cp1, ColorPoint cp2) {
      assertEquals(cp2.equals(cp1), cp1.equals(cp2));
    }

    @ParameterizedTest(name = "{index}: Transitive ColorPoint {0} {1} {2}")
    @MethodSource("transitiveProvider")
    public void transitivity(ColorPoint cp1, ColorPoint cp2, ColorPoint cp3) {
      if (cp1.equals(cp2) && cp2.equals(cp3)) {
         assertTrue(cp1.equals(cp3));
      }
    }


    static Stream<Arguments> reflexiveProvider() {
        return Stream.of(
                Arguments.of(new ColorPoint(1,2,Color.RED)),
                Arguments.of(new ColorPoint(2,2,Color.BLUE)),
                Arguments.of(new ColorPoint(3,1,Color.WHITE)),
                Arguments.of(new ColorPoint(0,0,Color.RED))
        );
    }

    static Stream<Arguments> symmetryProvider() {
        return Stream.of(
                Arguments.of(new ColorPoint(1,2,Color.RED), new ColorPoint(1,2,Color.RED)),
                Arguments.of(new ColorPoint(2,2,Color.BLUE), new ColorPoint(2,2,Color.BLUE)),
                Arguments.of(new ColorPoint(2,2,Color.WHITE), new ColorPoint(2,2,Color.BLUE)),
                Arguments.of(new ColorPoint(0,0,Color.BLUE), new ColorPoint(2,2,Color.BLUE)),
                Arguments.of(new ColorPoint(0,0,Color.BLUE), new ColorPoint(2,2,Color.RED))
        );
    }

   static Stream<Arguments> transitiveProvider() {
        return Stream.of(
                Arguments.of(new ColorPoint(1,2,Color.RED), new ColorPoint(1,2,Color.RED), new ColorPoint(1,2,Color.RED)),
                Arguments.of(new ColorPoint(2,2,Color.BLUE), new ColorPoint(2,2,Color.BLUE), new ColorPoint(2,2,Color.RED)),
                Arguments.of(new ColorPoint(2,2,Color.BLUE), new ColorPoint(2,2,Color.RED), new ColorPoint(2,2,Color.BLUE)),
                Arguments.of(new ColorPoint(2,2,Color.RED), new ColorPoint(2,2,Color.BLUE), new ColorPoint(2,2,Color.BLUE)),
                Arguments.of(new ColorPoint(0,0,Color.BLUE), new ColorPoint(2,2,Color.RED), new ColorPoint(1,2,Color.RED))
        );
    }
}
