package testing.pbt.practico.ejercicio6;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;


import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

//Introduction to Software Testing
//Authors: Paul Ammann & Jeff Offutt

/*
 * https://agiledeveloper.com/articles/equals062002.htm
 * Segun este articulo las propiedades que debe cumplir el equals
 * es la: reflexividad, simetria y transitividad.
 *
*/

public class PointTest {

    @ParameterizedTest(name = "{index}: Reflexive Point {0}")
    @MethodSource("reflexiveProvider")
    public void reflexive(Point p){
        assertTrue(p.equals(p));
    }

    @ParameterizedTest(name = "{index}: Symmetry Point {0} {1}")
    @MethodSource("symmetryProvider")
    public void symmetry(Point cp1, Point cp2) {
      assertEquals(cp2.equals(cp1), cp1.equals(cp2));
    }

    @ParameterizedTest(name = "{index}: Transitive Point {0} {1} {2}")
    @MethodSource("transitiveProvider")
    public void transitivity(Point cp1, Point cp2, Point cp3) {
      if (cp1.equals(cp2) && cp2.equals(cp3)) {
         assertTrue(cp1.equals(cp3));
      }
    }


    static Stream<Arguments> reflexiveProvider() {
        return Stream.of(
                Arguments.of(new Point(1,2)),
                Arguments.of(new Point(2,2)),
                Arguments.of(new Point(3,1)),
                Arguments.of(new Point(0,0))
        );
    }

    static Stream<Arguments> symmetryProvider() {
        return Stream.of(
                Arguments.of(new Point(1,2), new Point(1,2)),
                Arguments.of(new Point(2,3), new Point(2,3)),
                Arguments.of(new Point(3,2), new Point(3,2)),
                Arguments.of(new Point(0,0), new Point(2,2)),
                Arguments.of(new Point(0,0), new Point(0,0))
        );
    }

   static Stream<Arguments> transitiveProvider() {
        return Stream.of(
                Arguments.of(new Point(1,2), new Point(1,2), new Point(1,2)),
                Arguments.of(new Point(1,2), new Point(2,2), new Point(2,2)),
                Arguments.of(new Point(2,2), new Point(3,2), new Point(2,2)),
                Arguments.of(new Point(2,2), new Point(2,2), new Point(3,2)),
                Arguments.of(new Point(0,0), new Point(2,2), new Point(1,2))
        );
    }
}
