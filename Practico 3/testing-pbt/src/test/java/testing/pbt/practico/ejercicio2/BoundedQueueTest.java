package testing.pbt.practico.ejercicio2;

import static org.junit.jupiter.api.Assertions.*;

import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class BoundedQueueTest{

   private BoundedQueue queue;   // Test fixture

   /*@BeforeEach
   public void setUp() {
      queue = new BoundedQueue(10);
   }

   @AfterEach      // Tear down - Called after every test method.
   public void tearDown() {
      queue = null;  // redundant in this example!
   }*/

	@ParameterizedTest(name = "{index}: In queue {0} the result of dequeue after enqueue {1} must be {2}")
	@MethodSource("queueProvider")
	void dequeueEnqueueTest(BoundedQueue b, int elem, int expected) {
		b.enQueue(elem);
		assertEquals(expected, b.deQueue());
	}
	
	static Stream<Arguments> queueProvider() {
		
		BoundedQueue q1 = new BoundedQueue(5);
		for (int i = 0; i < 4; i++)	q1.enQueue(i);

		BoundedQueue q2 = new BoundedQueue(3);

		BoundedQueue q3 = new BoundedQueue(5);
		for (int i = 1; i < 3; i++)	q3.enQueue(i);

	    return Stream.of(
	    		Arguments.of(q1, 7, 0),
	    		Arguments.of(q2, 2, 2),
	    		Arguments.of(q3, 0, 1)
	    );
	}
	
	@ParameterizedTest(name = "{index}: Queue {0}: full=?{1} empty=?{2} ")
	@MethodSource("queuefulloremptyProvider")
	void queuefulloremptyTest(BoundedQueue b, boolean f, boolean e) {
		assertTrue(b.isFull() == f && b.isEmpty() == e);
	}
	
	static Stream<Arguments> queuefulloremptyProvider() {
		
		BoundedQueue q1 = new BoundedQueue(5);
		for (int i = 0; i < 5; i++)	q1.enQueue(i);

		BoundedQueue q2 = new BoundedQueue(3);

		BoundedQueue q3 = new BoundedQueue(5);
		for (int i = 1; i < 3; i++)	q3.enQueue(i);

		BoundedQueue q4 = new BoundedQueue(0);

	    return Stream.of(
	    		Arguments.of(q1, true, false),
	    		Arguments.of(q2, false, true),
	    		Arguments.of(q3, false, false),
	    		Arguments.of(q4, true, true)
	    );
	}

}
